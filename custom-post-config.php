<?php 	$custom_post_images = new CustomPostImages();

	/**
	 * Gallery Metabox - Only show on 'page' and 'rotator' post types
	 * @author Bill Erickson
	 * @link http://www.wordpress.org/extend/plugins/gallery-metabox
	 * @link http://www.billerickson.net/code/gallery-metabox-custom-post-types
	 * @since 1.0
	 *
	 * @param array $post_types
	 * @return array
	 */
	function be_gallery_metabox_page_and_rotator( $post_types ) {
		return array( 'payment' );
	}
	add_action( 'be_gallery_metabox_post_types', 'be_gallery_metabox_page_and_rotator' );

	function be_gallery_metabox_page_title_change( $title ) {
		return 'imagesk';
	}
	add_action( 'k99_gallery_metabox_title', 'be_gallery_metabox_page_title_change' );

	function be_gallery_metabox_page_array( $images ) {
		// return array(
			// '0' => array(
				// 'title' => 'Page Header',
				// 'slug' => 'header'
			// ),
			// '1' => array(
				// 'title' => 'Featured Image',
				// 'slug' => 'featured'
			// )
		// );
		// return '5';
		return 'att';
	}
	add_action( 'k99_gallery_metabox_post_array', 'be_gallery_metabox_page_array' );

 ?>
