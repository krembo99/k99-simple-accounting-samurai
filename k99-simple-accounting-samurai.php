<?php 
/*
* Plugin Name: k99 Simple Accounting Samurai
* Plugin URI: http://www.krembo99.com
* Description: Custom simple Accounting plugin for shneor
* Version: 0.4.1
* Author: Krembo99
* Author URI: http://www.krembo99.com
* Requires at least: 3.5
* Tested up to: 4.4.1
* Text Domain: k99-shneor-domain
* Domain Path: /languages
* 
* Bitbucket Plugin URI: https://bitbucket.org/krembo99/k99-simple-accounting-samurai
* Bitbucket Branch:     master
* 
*/


// Changelog :

// 0.4.1 - took down editor. Added custom images metabox
// 03.5 - Fixed sanitation add teextdomain
// 03.3 - Enabled Editor s
// 03.2 - Fixed thumbnail support ( not featured_image )
// 03.1 - Added featured image support , Invoice Number .


// don't load directly

if (!function_exists('is_admin')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}


include('custom-post-images.php');
include('custom-post-config.php');




function k99_shneor_payment_my_enqueue($hook) {
    // if ( 'edit.php' != $hook ) {
        // return;
    // }

  
wp_enqueue_script('jquery-ui-datepicker');
wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
wp_enqueue_script( 'k99-simple-accounting-samurai-init', plugins_url( '/js/k99-simple-accounting-samurai-init.js' , __FILE__ ), array( 'jquery-ui-datepicker' ),date ("Ymd", filemtime( __FILE__ )),true );
}
add_action( 'admin_enqueue_scripts', 'k99_shneor_payment_my_enqueue' );

function k99_simple_account_samurai_load_plugin_textdomain() {
    load_plugin_textdomain( 'k99-shneor-domain', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'k99_simple_account_samurai_load_plugin_textdomain' );


/*
*
*	Custom admin columns
*
*/

// ONLY PAYMENT CUSTOM TYPE POSTS
add_filter('manage_payment_posts_columns', 'ST4_columns_head_only_movies', 10);
add_action('manage_payment_posts_custom_column', 'ST4_columns_content_only_movies', 10, 2);
 
// CREATE TWO FUNCTIONS TO HANDLE THE COLUMN
function ST4_columns_head_only_movies($defaults) {
    $defaults['k99_shneor_payment_amount_column'] = __('Amount');
    $defaults['k99_shneor_payment_currency_column'] = __('Currency','k99-shneor-domain');
    $defaults['k99_shneor_payment_date_column'] = __('Date','k99-shneor-domain');

    $defaults['k99_shneor_payment_payee_column'] = __('Payee','k99-shneor-domain');
	$defaults['k99_shneor_payment_account_column'] = __('Acount','k99-shneor-domain');
	$defaults['k99_shneor_payment_remarks_column'] = __('Remarks','k99-shneor-domain');
    // $defaults['directors_name'] = 'Director';
    return $defaults;
}

function ST4_columns_content_only_movies($column_name, $post_ID) {
    // if ($column_name == 'directors_name') {
        // show content of 'directors_name' column
    // }
	if ($column_name == 'k99_shneor_payment_amount_column') {
        // show content of 'directors_name' column
		echo get_post_meta( $post_ID, 'k99-shneor-payment-amount', true );
		// echo 'yeyyyy';
    }
	if ($column_name == 'k99_shneor_payment_payee_column') {
        // show content of 'directors_name' column
		echo get_post_meta( $post_ID, 'k99-shneor-payment-payee', true );
    }
	if ($column_name == 'k99_shneor_payment_date_column') {
        // show content of 'directors_name' column
		echo get_post_meta( $post_ID, 'k99-shneor-payment-date', true );
    }
	if ($column_name == 'k99_shneor_payment_currency_column') {
        // show content of 'directors_name' column
		$currency = get_post_meta( $post_ID, 'k99-shneor-payment-currency', true );
		if ( $currency == 'Dollar'){
			$cur = '$';
			
		} elseif  ( $currency == 'Shekel') {
			$cur = '&#8362;';

		}
		echo $cur;
    }
	if ($column_name == 'k99_shneor_payment_account_column') {
        // show content of 'directors_name' column
		echo get_post_meta( $post_ID, 'k99-shneor-payment-account', true );
    }
	if ($column_name == 'k99_shneor_payment_remarks_column') {
        // show content of 'directors_name' column
		echo get_post_meta( $post_ID, 'k99-shneor-payment-remarks', true );
    }
}


/*
*
*	Custom Post type
*
*/

function k99_simple_account_samurai_payment_custom_init() {
	// empty support cleans the metaboxes
	//http://wordpress.stackexchange.com/a/6858/13539
	$supports = array ('title','thumbnail','custom-fields'); // took down editor ver 03.6 
    $args = array(
      'public' => true,
      'label'  => __('Payments','k99-shneor-domain'),
		'supports' => $supports,
	  'hierarchical' => false,
      'public' => true,
      'rewrite' => true,
	  'menu_icon'   => 'dashicons-money',
	  'taxonomies' => array('post_tag'),
    );
    register_post_type( 'payment', $args );
}
add_action( 'init', 'k99_simple_account_samurai_payment_custom_init' );


/*
*
*	Remove menus ( post, pages )
*
*/

function k99_simple_account_samurai_remove_menus () {
global $menu;
	$restricted = array(__('Posts'),  __('Pages'));
	end ($menu);
	while (prev($menu)){
		$value = explode(' ',$menu[key($menu)][0]);
		if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
	}
}
add_action('admin_menu', 'k99_simple_account_samurai_remove_menus');	



/*
*
*	Remove unwanted metaboxes ( probably no need because removed support from CPT )
*
*/


if (is_admin()) :
function k99_simple_account_samurai_meta_boxes() {
 // if( !current_user_can('manage_options') ) {
  remove_meta_box('linktargetdiv', 'link', 'normal');
  remove_meta_box('linkxfndiv', 'link', 'normal');
  remove_meta_box('linkadvanceddiv', 'link', 'normal');
  remove_meta_box('postexcerpt', 'post', 'normal');
  remove_meta_box('trackbacksdiv', 'post', 'normal');
  remove_meta_box('postcustom', 'post', 'normal');
  remove_meta_box('commentstatusdiv', 'post', 'normal');
  remove_meta_box('commentsdiv', 'post', 'normal');
  remove_meta_box('revisionsdiv', 'post', 'normal');
  remove_meta_box('authordiv', 'post', 'normal');
  remove_meta_box('sqpt-meta-tags', 'post', 'normal');
  remove_meta_box( 'slugdiv', 'post', 'normal' );
	remove_meta_box( 'categorydiv','post','normal' ); // Categories Metabox
	remove_meta_box( 'formatdiv','post','normal' ); // Formats Metabox
	// remove_meta_box( 'postimagediv','post','normal' ); // Featured Image Metabox
	remove_meta_box( 'submitdiv','post','normal' ); // Categories Metabox
	remove_meta_box( 'tagsdiv-post_tag','post','normal' ); // Tags Metabox

// remove_meta_box( 'postimagediv','post','normal' );

// remove_post_type_support('post', 'editor');
 // }
}
// add_action( 'admin_menu', 'k99_simple_account_samurai_meta_boxes' );
endif;


/*
*
*	Custom meta box
*
*/

/* Fire our meta box setup function on the post editor screen. */
add_action( 'load-post.php', 'k99_simple_accounting_samurai_post_meta_boxes_setup' );
add_action( 'load-post-new.php', 'k99_simple_accounting_samurai_post_meta_boxes_setup' );

/* Meta box setup function. */
function k99_simple_accounting_samurai_post_meta_boxes_setup() {

  /* Add meta boxes on the 'add_meta_boxes' hook. */
  add_action( 'add_meta_boxes', 'k99_simple_accounting_samurai_add_post_meta_boxes' );
  
  /* Save post meta on the 'save_post' hook. */
  add_action( 'save_post', 'k99_shneor_save_post_meta', 10, 2 );
}

/* Create one or more meta boxes to be displayed on the post editor screen. */
function k99_simple_accounting_samurai_add_post_meta_boxes() {

  add_meta_box(
    'smashing-post-class',      // Unique ID
    esc_html__( 'Payment', 'k99-shneor-domain' ),    // Title
    'k99_shneor_account_meta_box',   // Callback function
    'payment',         // Admin page (or post type)
    'normal',         // Context
    'default'         // Priority
  );
}


/* Display the post meta box. */
function k99_shneor_account_meta_box( $object, $box ) { ?>

  <?php wp_nonce_field( basename( __FILE__ ), 'k99-shneor-account_nonce' ); ?>


  <p>
    <label for="k99-shneor-payment-payee"><?php _e( "Insert Payee.", 'k99-shneor-domain' ); ?></label>
    <br />
    <input class="widefat" type="text" name="k99-shneor-payment-payee" id="k99-shneor-payment-payee" value="<?php echo esc_attr( get_post_meta( $object->ID, 'k99-shneor-payment-payee', true ) ); ?>" size="30" />
  </p>
  <p>
    <label for="k99-shneor-payment-date"><?php _e( "Insert date.", 'k99-shneor-domain' ); ?></label>
    <br />
    <input class="widefat" type="text" name="k99-shneor-payment-date" id="k99-shneor-payment-date" value="<?php echo esc_attr( get_post_meta( $object->ID, 'k99-shneor-payment-date', true ) ); ?>" size="30" />
  </p>
  <p>
    <label for="k99-shneor-payment-amount"><?php _e( "Insert Amount.", 'k99-shneor-domain' ); ?></label>
    <br />
    <input class="widefat" type="text" name="k99-shneor-payment-amount" id="k99-shneor-payment-amount" value="<?php echo esc_attr( get_post_meta( $object->ID, 'k99-shneor-payment-amount', true ) ); ?>" size="30" />
  </p>

   <label for="k99-shneor-payment-currency"> <?php _e( "Insert Currency.", 'k99-shneor-domain' ); ?> </label>
   <br />
    <select name="k99-shneor-payment-currency" id="k99-shneor-payment-currency" class="postbox">
        <option value="Shekel"><?php _e( "&#8362;", 'k99-shneor-domain' ); ?></option>
        <option value="Dollar"><?php _e( "$", 'k99-shneor-domain' ); ?></option>
        <option value="CNY">Else</option>
    </select>
  
  <p>
    <label for="k99-shneor-payment-account"><?php _e( "Insert account.", 'k99-shneor-domain' ); ?></label>
    <br />
    <input class="widefat" type="text" name="k99-shneor-payment-account" id="k99-shneor-payment-account" value="<?php echo esc_attr( get_post_meta( $object->ID, 'k99-shneor-payment-account', true ) ); ?>" size="30" />
  </p>  
  <p>
    <label for="k99-shneor-payment-invoice"><?php _e( "Insert Invoice No.", 'k99-shneor-domain' ); ?></label>
    <br />
    <input class="widefat" type="text" name="k99-shneor-payment-invoice" id="k99-shneor-payment-invoice" value="<?php echo esc_attr( get_post_meta( $object->ID, 'k99-shneor-payment-invoice', true ) ); ?>" size="30" />
  </p> 
  <p>
    <label for="k99-shneor-payment-type"><?php _e( "Insert type ", 'k99-shneor-domain' ); ?></label>
    <br />
    <input class="widefat" type="text" name="k99-shneor-payment-type" id="k99-shneor-payment-type" value="<?php echo esc_attr( get_post_meta( $object->ID, 'k99-shneor-payment-type', true ) ); ?>" size="30" />
  </p>

  <p>
    <label for="k99-shneor-payment-remarks"><?php _e( "Insert Remarks.", 'k99-shneor-domain' ); ?></label>
    <br />
	<textarea class="widefat" rows="4" cols="4" name="k99-shneor-payment-remarks"  id="k99-shneor-payment-remarks" class="inside"><?php echo esc_attr( get_post_meta( $object->ID, 'k99-shneor-payment-remarks', true ) ); ?></textarea>
	</p>
   

<?php }


/* Save post meta on the 'save_post' hook. */
add_action( 'save_post', 'k99_shneor_save_post_meta', 10, 2 );

/* Save the meta box's post metadata. */
function k99_shneor_save_post_meta( $post_id, $post ) {

  /* Verify the nonce before proceeding. */
  if ( !isset( $_POST['k99-shneor-account_nonce'] ) || !wp_verify_nonce( $_POST['k99-shneor-account_nonce'], basename( __FILE__ ) ) )
    return $post_id;

  /* Get the post type object. */
  $post_type = get_post_type_object( $post->post_type );

  /* Check if the current user has permission to edit the post. */
  if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
    return $post_id;

  /* Get the posted data and sanitize it for use as an HTML class. */
  $new_meta_value = ( isset( $_POST['smashing-post-class'] ) ? sanitize_html_class( $_POST['smashing-post-class'] ) : '' );

  
  $keys = array(
  'k99-shneor-payment-payee',
  'k99-shneor-payment-date',
  'k99-shneor-payment-amount',
  'k99-shneor-payment-currency',
  'k99-shneor-payment-account',
  'k99-shneor-payment-type',
  'k99-shneor-payment-invoice',
  'k99-shneor-payment-remarks',
  'k99-shneor-payment-type',
  
  );
  foreach ($keys as $key){
  /* Get the meta key. */
  // $meta_key = 'k99-shneor-account';
  $meta_key = $key;
  
  
  // $new_meta_value = ( isset( $_POST['smashing-post-class'] ) ? sanitize_html_class( $_POST['smashing-post-class'] ) : '' );
  $new_meta_value = ( isset( $_POST[$key] ) ? esc_textarea( $_POST[$key] ) : '' );

  /* Get the meta value of the custom field key. */
  $meta_value = get_post_meta( $post_id, $meta_key, true );

  /* If a new meta value was added and there was no previous value, add it. */
  if ( $new_meta_value && '' == $meta_value )
    add_post_meta( $post_id, $meta_key, $new_meta_value, true );

  /* If the new meta value does not match the old value, update it. */
  elseif ( $new_meta_value && $new_meta_value != $meta_value )
    update_post_meta( $post_id, $meta_key, $new_meta_value );

  /* If there is no new meta value but an old value exists, delete it. */
  elseif ( '' == $new_meta_value && $meta_value )
    delete_post_meta( $post_id, $meta_key, $meta_value );
	
	}
}

?>